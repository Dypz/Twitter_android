package worldline.ssm.rd.ux.wltwitter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import worldline.ssm.rd.ux.wltwitter.async.RetriveTweetsAsyncTask;
import worldline.ssm.rd.ux.wltwitter.interfaces.TweetListener;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.ui.fragments.TweetDetailsFragment;
import worldline.ssm.rd.ux.wltwitter.ui.fragments.TweetsFragment;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;

public class WLTwitterActivity extends Activity implements TweetListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wltwitter);
        String login = getIntent().getExtras().getString(Constants.Login.EXTRA_LOGIN,null);
        if(login == null)
            return;
        getActionBar().setSubtitle(login);

        if(savedInstanceState == null){
            getFragmentManager().beginTransaction().add(R.id.container, new TweetsFragment()).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.wltwitter,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case(R.id.action_logout):
                SharedPreferences sharedPref = this.getSharedPreferences(Constants.Preferences.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
                // remove login and password from preferences
                sharedPref.edit().remove(Constants.Preferences.PREF_LOGIN).commit();
                sharedPref.edit().remove(Constants.Preferences.PREF_PASSWORD).commit();
                // finish this activity and go back to the previous activity : here login activity
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onViewTweet(Tweet tweet){
        final TweetDetailsFragment tweetDetailsFragment = TweetDetailsFragment.newInstance(tweet.text, tweet.user.name, tweet.user.screenName, tweet.user.profileImageUrl);
        this.getFragmentManager().beginTransaction().replace(R.id.container, tweetDetailsFragment, null).addToBackStack(null).commit();
    }

    @Override
    public void onRetweet(Tweet tweet){

    }

}
