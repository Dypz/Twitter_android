package worldline.ssm.rd.ux.wltwitter.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import worldline.ssm.rd.ux.wltwitter.databases.WLTwitterDatabaseHelper;
import worldline.ssm.rd.ux.wltwitter.databases.WLTwitterDatabseContract;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;

/**
 * Created by aubin on 21/10/2016.
 */

public class WLTwitterDatabaseProvider extends ContentProvider {

    private static final int TWEET_CORRECT_URI_CODE = 42;
    private WLTwitterDatabaseHelper mDBHelper;
    private UriMatcher mUriMatcher;
    @Override
    public boolean onCreate() {
        mDBHelper = new WLTwitterDatabaseHelper(getContext());
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(WLTwitterDatabseContract.CONTENT_PROVIDER_TWEETS_AUTHORITY, WLTwitterDatabseContract.TABLE_TWEETS, TWEET_CORRECT_URI_CODE);
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.w(Constants.General.LOG_TAG, "QUERY");
        SQLiteDatabase db = mDBHelper.getReadableDatabase();
        Cursor c = db.query(WLTwitterDatabseContract.TABLE_TWEETS, projection, selection, selectionArgs, sortOrder, null, null);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        if(mUriMatcher.match(uri) == TWEET_CORRECT_URI_CODE){
            return WLTwitterDatabseContract.TWEETS_CONTENT_TYPE;
        }
        throw new IllegalArgumentException("Unknown URI : "+uri);
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Log.i(Constants.General.LOG_TAG, "INSERT");
        /*final SQLiteDatabase db = mDBHelper.getWritableDatabase();
        final long rowID = db.insert(WLTwitterDatabseContract.TABLE_TWEETS,"",values);
        if(rowID > 0) {
            final Uri applicationUri = ContentUris.withAppendedId(WLTwitterDatabseContract.TWEETS_URI, rowID);
            getContext().getContentResolver().notifyChange(applicationUri, null);
            return applicationUri;
        }*/
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        Log.i(Constants.General.LOG_TAG, "DELETE");
        /*String id = null;
        if(mUriMatcher.match(uri) == TWEET_CORRECT_URI_CODE){
            id = uri.getPathSegments().get(1);
            return mDBHelper.getWritableDatabase().delete(WLTwitterDatabseContract.TABLE_TWEETS,"_ID=?",new String[]{id});
        }
        else{
            return mDBHelper.getWritableDatabase().delete(WLTwitterDatabseContract.TABLE_TWEETS,null,null);
        }*/
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.i(Constants.General.LOG_TAG, "UPDATE");
        /*String id = null;
        if(mUriMatcher.match(uri) == TWEET_CORRECT_URI_CODE){
            Log.d("test", uri.getPathSegments().toString());
            id = uri.getPathSegments().get(0);
            return mDBHelper.getWritableDatabase().update(WLTwitterDatabseContract.TABLE_TWEETS,values,"_ID=?",new String[]{id});
        }
        else{
            return mDBHelper.getWritableDatabase().update(WLTwitterDatabseContract.TABLE_TWEETS,values,null,null);
        }*/
        return 0;
    }
}
