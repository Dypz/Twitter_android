package worldline.ssm.rd.ux.wltwitter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.Console;

import worldline.ssm.rd.ux.wltwitter.utils.Constants;

/**
 * Created by aubin on 23/09/2016.
 */
public class WLTwitterLoginActivity extends Activity implements View.OnClickListener {

    private static final String keyLogin = Constants.Preferences.PREF_LOGIN;
    private static final String keyPassword = Constants.Preferences.PREF_PASSWORD;
    private static final String extrasKey = Constants.Login.EXTRA_LOGIN;
    private static final String fileName = Constants.Preferences.SHARED_PREFERENCES_FILE_NAME;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        tryUserAlreadyLogin();
        findViewById(R.id.loginButton).setOnClickListener(this);
    }

    // method to check if a user already login <=> login and password exist in preferences
    private void tryUserAlreadyLogin(){
        SharedPreferences sharedPref = this.getSharedPreferences(fileName, Context.MODE_PRIVATE);

        // we give a default value to null if don't find in preferences
        String login = sharedPref.getString(keyLogin, null);
        String password = sharedPref.getString(keyPassword, null);

        if(login == null || password == null) return;
        // if the user is already login, we start the twitter activity
        twitterIntent(login);
    }

    @Override
    public void onClick(View view){
        EditText loginText = (EditText)findViewById(R.id.loginEditText);
        EditText passwordText = (EditText)findViewById(R.id.passwordEditText);

        String login = loginText.getText().toString();
        String password = passwordText.getText().toString();

        if(TextUtils.isEmpty(login) || TextUtils.isEmpty(password) ){
            // an edit text area is empty
            // we launch a toast to the user
            Toast.makeText(this,"The fields login and password must not be empty ! ", Toast.LENGTH_LONG).show();
        }
        else{
            // store login/password with SharedPreferences
            SharedPreferences prefs =  this.getSharedPreferences(fileName, Context.MODE_PRIVATE);

            prefs.edit().putString(keyLogin,login).commit();
            prefs.edit().putString(keyPassword,password).commit();

            twitterIntent(login);
        }
    }

    // method to start the twitter activity
    private void twitterIntent(String login){
        Intent twitterIntent = new Intent(this, WLTwitterActivity.class);
        Bundle extras = new Bundle();
        extras.putString(extrasKey,login);
        twitterIntent.putExtras(extras);
        startActivity(twitterIntent);
    }


    public static String getExtrasKey() {
        return extrasKey;
    }

    public static String getKeyLogin() {
        return keyLogin;
    }

    public static String getKeyPassword() {
        return keyPassword;
    }

    public static String getFileName(){
        return fileName;
    }

}
