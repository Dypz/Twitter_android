package worldline.ssm.rd.ux.wltwitter.adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.async.ImageDownloaderAsyncTask;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by aubin on 07/10/2016.
 */
public class TweetsAdapter extends BaseAdapter{

    private List<Tweet> mTweets;
    private LayoutInflater mInflater;
    private Context mContext;

    public TweetsAdapter(List<Tweet> tweets, Context context){
        mTweets = tweets;
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mTweets != null ? mTweets.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mTweets != null ? mTweets.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.tweet_listitem, null);
            holder = new ViewHolder(convertView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        // Get current item
        final Tweet tweet = (Tweet) getItem(position);
        // Set differents infos
        holder.name.setText(tweet.user.name);
        holder.alias.setText(tweet.user.screenName);
        holder.content.setText(tweet.text);
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mInflater.getContext(), "Retweet :" + holder.content.getText(), Toast.LENGTH_LONG).show();
            }
        });

        // we can also use the AsyncTask we have implemented
        new ImageDownloaderAsyncTask(holder.image).execute(tweet.user.profileImageUrl);
        //Picasso.with(mContext).load(tweet.user.profileImageUrl).into(holder.image);

        return convertView;
    }
}
