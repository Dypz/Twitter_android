package worldline.ssm.rd.ux.wltwitter.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.async.ImageDownloaderAsyncTask;


public class TweetDetailsFragment extends Fragment implements View.OnClickListener {

    private static final String CONTENT_KEY = "content_key";
    private static final String USERNAME_KEY = "username_key";
    private static final String ALIAS_KEY = "alias_key";
    private static final String IMAGEURL_KEY = "imageURL_key";


    public TweetDetailsFragment() {
        // Required empty public constructor
    }

    public static TweetDetailsFragment newInstance(String content, String username, String alias, String profileImageURL) {
        TweetDetailsFragment fragment = new TweetDetailsFragment();
        Bundle args = new Bundle();
        args.putString(CONTENT_KEY, content);
        args.putString(USERNAME_KEY, username);
        args.putString(ALIAS_KEY, alias);
        args.putString(IMAGEURL_KEY, profileImageURL);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String content = getArguments().getString(CONTENT_KEY);
        String username = getArguments().getString(USERNAME_KEY);
        String alias = getArguments().getString(ALIAS_KEY);
        String profileImageURL = getArguments().getString(IMAGEURL_KEY);

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_tweet_details, container, false);

        TextView contentView = (TextView)rootView.findViewById(R.id.contentTweetDetail);
        contentView.setText(content);
        TextView usernameView = (TextView)rootView.findViewById(R.id.usernameTweetDetail);
        usernameView.setText(username);
        TextView aliasView = (TextView)rootView.findViewById(R.id.aliasTweetDetail);
        aliasView.setText(alias);

        new ImageDownloaderAsyncTask((ImageView) rootView.findViewById(R.id.imageTweetDetail)).execute(profileImageURL);

        Button retweetButton = (Button) rootView.findViewById(R.id.retweetButton);
        retweetButton.setOnClickListener(this);
        Button replyButton = (Button) rootView.findViewById(R.id.replyTweetButton);
        replyButton.setOnClickListener(this);
        Button starButton = (Button) rootView.findViewById(R.id.starTweetButton);
        starButton.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        String content = getArguments().getString(CONTENT_KEY);
        switch (v.getId()){
            case R.id.retweetButton:
                Toast.makeText(getActivity(), "Retweet "+content, Toast.LENGTH_LONG).show();
                break;
            case R.id.replyTweetButton:
                Toast.makeText(getActivity(), "Reply "+content, Toast.LENGTH_LONG).show();
                break;
            case R.id.starTweetButton:
                Toast.makeText(getActivity(), "Star "+content, Toast.LENGTH_LONG).show();
                break;

        }
    }
}
