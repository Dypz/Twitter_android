package worldline.ssm.rd.ux.wltwitter.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by aubin on 21/10/2016.
 */

public class WLTwitterDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABSE_NAME = "tweets.db";
    private static final int DATABASE_VERSION = 1;

    public WLTwitterDatabaseHelper(Context context){
        super(context, DATABSE_NAME, null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(WLTwitterDatabseContract.TABLE_TWEETS_CREATE_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP IF TABLE EXISTS "+ WLTwitterDatabseContract.TABLE_TWEETS);

        onCreate(db);
    }
}
