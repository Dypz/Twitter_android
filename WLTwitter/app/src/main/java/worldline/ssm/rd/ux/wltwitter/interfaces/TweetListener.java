package worldline.ssm.rd.ux.wltwitter.interfaces;

import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by aubin on 04/10/2016.
 */
public interface TweetListener {

    public void onRetweet(Tweet tweet);
    public void onViewTweet(Tweet tweet);
}
