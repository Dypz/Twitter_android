package worldline.ssm.rd.ux.wltwitter.async;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;


import com.squareup.picasso.Picasso;

import worldline.ssm.rd.ux.wltwitter.helpers.TwitterHelper;

/**
 * Created by aubin on 10/10/2016.
 */

public class ImageDownloaderAsyncTask extends AsyncTask<String, Void, Bitmap> {

    private ImageView mImageView;

    public ImageDownloaderAsyncTask(ImageView imageView){
        mImageView = imageView;
    }

    @Override
    protected Bitmap doInBackground(String... urlImage) {
        if(urlImage == null) return null;
        try{
            return TwitterHelper.getTwitterUserImage(urlImage[0]);

        }
        catch (Exception e){
            Log.e("ImageDownLoaderTask","Enable to load the image with the given URL");
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if(bitmap != null)
            mImageView.setImageBitmap(bitmap);
    }
}
