package worldline.ssm.rd.ux.wltwitter.interfaces;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by aubin on 30/09/2016.
 */
public interface TweetChangeListener {
    public void onTweetRetrieved(List<Tweet> tweets);
}
