package worldline.ssm.rd.ux.wltwitter.ui.fragments;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.WLTwitterActivity;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.adapters.TweetsAdapter;
import worldline.ssm.rd.ux.wltwitter.adapters.TweetsCursorAdapter;
import worldline.ssm.rd.ux.wltwitter.async.RetriveTweetsAsyncTask;
import worldline.ssm.rd.ux.wltwitter.databases.WLTwitterDatabaseHelper;
import worldline.ssm.rd.ux.wltwitter.databases.WLTwitterDatabaseManager;
import worldline.ssm.rd.ux.wltwitter.databases.WLTwitterDatabseContract;
import worldline.ssm.rd.ux.wltwitter.interfaces.TweetChangeListener;
import worldline.ssm.rd.ux.wltwitter.interfaces.TweetListener;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.utils.PreferenceUtils;

/**
 * Created by aubin on 30/09/2016.
 */
public class TweetsFragment extends Fragment implements TweetChangeListener, AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private ListView mListView;
    private RetriveTweetsAsyncTask mTweetsAsyncTask;
    private TweetListener mListener;

    public TweetsFragment(){}

    @Override
    public void onStart(){

        super.onStart();
        final String login = PreferenceUtils.getLogin(getActivity().getApplicationContext());
        if(!TextUtils.isEmpty(login)){
            mTweetsAsyncTask = new RetriveTweetsAsyncTask(this);
            mTweetsAsyncTask.execute(login);
        }
    }

    // override method to display a fragment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_wltwitter, container, false);

        // get the list view
        mListView = (ListView) rootView.findViewById(R.id.tweetsListView);
        mListView.setOnItemClickListener(this);

        // set a progress bar and an empty list view
        final ProgressBar progressBar = new ProgressBar(getActivity());
        progressBar.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                                    ActionBar.LayoutParams.WRAP_CONTENT,Gravity.CENTER));
        progressBar.setIndeterminate(true);
        mListView.setEmptyView(progressBar);

        // add the view on our content view
        ViewGroup root = (ViewGroup) rootView.findViewById(R.id.tweetsRootRelativeLayout);
        root.addView(progressBar);

        getLoaderManager().initLoader(0, null, this);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
         if(activity instanceof TweetListener){
             mListener = (TweetListener) activity;
         }
    }

    @Override
    public void onTweetRetrieved(List<Tweet> tweets){
        WLTwitterDatabaseManager twitterDatabaseManager = new WLTwitterDatabaseManager();
        twitterDatabaseManager.testDatabase(tweets,getActivity());
        twitterDatabaseManager.testContentProvider(getActivity());
        TweetsAdapter adapter = new TweetsAdapter(tweets, getActivity());
        mListView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
        if(mListener == null) return;
        final Tweet tweet = (Tweet)adapter.getItemAtPosition(position);
        mListener.onViewTweet(tweet);
    }

    // method to create CursorLoader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        final CursorLoader cursorLoader = new CursorLoader(getActivity());
        cursorLoader.setUri(WLTwitterDatabseContract.TWEETS_URI);
        cursorLoader.setProjection(WLTwitterDatabseContract.PROJECTION_FULL);
        cursorLoader.setSelection(null);
        cursorLoader.setSelectionArgs(null);
        cursorLoader.setSortOrder(null);
        return cursorLoader;
    }

    // method to get results
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(data != null){
            while(data.moveToNext()){
                final Tweet tweet = WLTwitterDatabaseManager.tweetFromCursor(data);
                Log.d("Tweets Fragment", tweet.toString());
            }
        }
        if(!data.isClosed()){
            data.close();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
}
