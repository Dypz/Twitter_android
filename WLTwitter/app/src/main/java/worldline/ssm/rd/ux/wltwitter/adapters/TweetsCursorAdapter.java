package worldline.ssm.rd.ux.wltwitter.adapters;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.Toast;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.async.ImageDownloaderAsyncTask;
import worldline.ssm.rd.ux.wltwitter.databases.WLTwitterDatabaseManager;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by aubin on 23/10/2016.
 */

public class TweetsCursorAdapter extends CursorAdapter {

    public TweetsCursorAdapter(Context context, Cursor c, int flags){
        super(context, c, flags);
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        Log.d("test","new view");
        final View view = LayoutInflater.from(context).inflate(R.layout.tweet_listitem,null);
        // View holder
        final ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final ViewHolder holder = (ViewHolder) view.getTag();
        final Tweet tweet = WLTwitterDatabaseManager.tweetFromCursor(cursor);
        holder.name.setText(tweet.user.name);
        holder.alias.setText(tweet.user.screenName);
        holder.content.setText(tweet.text);
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "Retweet :" + holder.content.getText(), Toast.LENGTH_LONG).show();
            }
        });

        // we can also use the AsyncTask we have implemented
        new ImageDownloaderAsyncTask(holder.image).execute(tweet.user.profileImageUrl);
    }
}
