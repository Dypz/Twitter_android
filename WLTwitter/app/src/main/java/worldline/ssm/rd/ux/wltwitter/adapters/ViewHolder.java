package worldline.ssm.rd.ux.wltwitter.adapters;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import worldline.ssm.rd.ux.wltwitter.R;

/**
 * Created by aubin on 23/10/2016.
 */

public class ViewHolder {
        public ImageView image;
        public TextView name;
        public TextView alias;
        public TextView content;
        public Button button;

        public ViewHolder(View view) {
            image = (ImageView) view.findViewById(R.id.tweetListItemImageView);
            name = (TextView) view.findViewById(R.id.tweetListItemUsernameView);
            alias = (TextView) view.findViewById(R.id.tweetListItemAliasView);
            content = (TextView) view.findViewById(R.id.tweetListItemContentView);
            button = (Button) view.findViewById(R.id.retweetButton);
        }
}
