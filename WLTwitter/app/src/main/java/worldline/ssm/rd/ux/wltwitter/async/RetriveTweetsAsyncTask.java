package worldline.ssm.rd.ux.wltwitter.async;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.helpers.TwitterHelper;
import worldline.ssm.rd.ux.wltwitter.interfaces.TweetChangeListener;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.utils.PreferenceUtils;

/**
 * Created by aubin on 30/09/2016.
 */
public class RetriveTweetsAsyncTask extends AsyncTask<String, Void, List<Tweet>> {

    private TweetChangeListener mListener;

    public RetriveTweetsAsyncTask(TweetChangeListener listener){
        mListener = listener;
    }

    protected List<Tweet> doInBackground(String...  login){
        if(login == null) return null;

        return TwitterHelper.getTweetsOfUser(login[0]);

    }


    protected void onPostExecute(List<Tweet> result) {
        if( mListener != null && result != null){
            mListener.onTweetRetrieved(result);
        }
    }

}
